//rules for naming variables

//you cannot start with a number
// example - 1value (invalid), value1 (valid)
var value1 = 10;
console.log(value1 / 2);

//you can use an underscore or dollar sign
// example - first_name(valid) , _firstname(valid)
// first$name(valid) , $firstname(valid)
var first_Name = "Anuja";
console.log(first_Name);

//you cannot use spaces
// example - first Name (invalid) 

//convention
//use with small letter and camelCase