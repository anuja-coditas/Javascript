const userMethods = {
    about : function(){
        return `${this.firstName} is ${this.age} years old.`;
    },
    is18 : function(){
        return this.age >= 18;
    },
    sing: function(){
        return 'hip hip hoorahhhhhh  ';
    }
}
function createUser(firstName, lastName, email, age, address){
    const user = Object.create(userMethods);// {}
    user.firstName = firstName;
    user.lastName = lastName;
    user.email = email;
    user.age = age;
    user.address = address;
    return user;
}

const user1 = createUser('anuja', 'Sharma', 'anuja@gmail.com', 22, "Pune");
const user2 = createUser('Shalini', 'Sharma', 'anuja@gmail.com', 24, "Jamshedpur");
const user3 = createUser('Purvi', 'Sharma', 'anuja@gmail.com', 19, "Ranchi");
console.log(user1);
console.log(user1.about());
console.log(user3.sing());