// undefined 
// null

let firstName;
console.log(typeof firstName);
firstName = "Anuja";
console.log(typeof firstName, firstName);

let myVariable = null;
console.log(myVariable);
myVariable = "anuja";
console.log(myVariable, typeof myVariable);
console.log(typeof null);
// bug , error 

// BigInt - primitive data type
let myNumber = BigInt(12);
let sameMyNumber = 123n; // also BigInt
console.log(myNumber);
console.log(Number.MAX_SAFE_INTEGER); // maximum safe no. in js
console.log(myNumber+ sameMyNumber);