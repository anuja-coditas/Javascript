// new keyword 
// 1.) this = {}
// 2.) return {} 

// __proto__ 
// // official ecmascript document
// [[prototype]]

// constructor function 
function CreateUser(firstName, lastName, email, age, address){
    this.firstName = firstName;
    this.lastName = lastName;
    this.email = email;
    this.age = age;
    this.address = address;
}
CreateUser.prototype.about = function(){
    return `${this.firstName} is ${this.age} years old.`;
};
CreateUser.prototype.is18 = function (){
    return this.age >= 18; 
}
CreateUser.prototype.sing = function (){
    return "la la la la ";
}


const user1 = new CreateUser('anuja', 'sharma', 'anuja@gmail.com', 18, "my address");
const user2 = new CreateUser('shalini', 'sharma', 'anuja@gmail.com', 19, "my address");
const user3 = new CreateUser('purvi', 'sharma', 'anuja@gmail.com', 17, "my address");
console.log(user2);
console.log(user2.is18());