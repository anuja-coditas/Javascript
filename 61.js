// find method 

const myArray = ["Hello", "cat", "dog", "lion"];

function isLength3(string){
return string.length === 3;
}

const ans = myArray.find((string)=>string.length===3);
console.log(ans);

const users = [
    {userId : 1, userName: "anuja"},
    {userId : 2, userName: "shalini"},
    {userId : 3, userName: "shivam"},
    {userId : 4, userName: "prateek"},
    {userId : 5, userName: "archi"},
];

const myUser = users.find((user)=>user.userId===3);
console.log(myUser);
