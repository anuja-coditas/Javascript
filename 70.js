// optional chaining 

const user  = {
    firstName: "Anuja",
    address: {house: 'Pune'}
}



console.log(user?.firstName);
console.log(user?.address?.house);