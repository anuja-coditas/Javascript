// if else condition 

let age = 22;

if(age>=18)
{
     console.log("User can play Pubg");
 }
 else 
 {
    console.log("User can play mario");
 }

 let num = 15;

 if(num%2==0){
console.log("even");
 }
 else
 {
 console.log("odd");
 }

// falsy values 
// false
// ""
// null 
// undefined
// 0

// truthy 
// "abc"
// 1, -1
 let firstName= "";

if(firstName)
{
 console.log(firstName);
}else{
 console.log("firstName is empty");
}