// trim()
// toUpperCase()
// toLowerCase()
// slice

let firstName = "   Anuja $  ";
console.log(firstName.length);

let newString = firstName.trim(); // "Anuja"
console.log(newString);

console.log(firstName);
console.log(newString.length);

console.log(firstName.toUpperCase());
// or firstName=firstName.toUpperCase());

console.log(firstName.toLowerCase());
// or firstName=firstName.toLowerCase());
//console.log(firstName);

//start index
//end index
//let newString1 = firstName.slice(0,2); // anu
console.log(firstName.slice(0,6));