//typeof operator

// data types
//string "Anuja"
//number 2,4,5.8
//booleans
//undefined
//null
//BigInt
//Symbol

let age = 22;
let firstName = "Anuja";

console.log(typeof age);
console.log(typeof firstName);

// convert number to string
// 22 -> "22";
age = age + "";
console.log(typeof age);
// or console.log(typeof (age + ""));

// convert string to number
let myStr = +"22";
console.log(typeof myStr);

let years = 18;
years = String(years);
console.log(typeof years);

let days = '6';
days = Number(days);
console.log(typeof days);
