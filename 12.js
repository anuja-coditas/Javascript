// booleans & comparison operator 

// booleans - true, false 

let num1 = 7;
let num2 = 5;

console.log(num1>=num2);

// == vs === 
let num3 = "7";
let num4 = 7;
console.log(num3 == num4);
console.log(num3 === num4);

 // != vs !==

console.log(num1 != num2);
console.log(num1 !== num2);