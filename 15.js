// ternary operator 

let age = 14;
let drink;

if(age>=10){
     drink = "coffee";
 }else{
     drink = "milk";
 }

 console.log(drink);

// ternary operator / conditional operator 

  age = 3;
 drink = age >= 10 ? "coffee" : "milk";
 console.log(drink);