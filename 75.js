// arrow functions 

const user1 = {
    firstName : "anuja",
    age: 22,
    about: () => {
        console.log(this.firstName, this.age);
    }   
}

user1.about(user1);            
