// map method 
const numbers = [3,4,6,1,8];

const square = function(number){
  return number*number;
 }

 const squareNumber = numbers.map((number, index)=>{
 return index;
 });
console.log(squareNumber);

const users = [
    {firstName: "anuja", age: 22},
    {firstName: "shalini", age: 24},
    {firstName: "shivam", age: 23},
    {firstName: "prateek", age: 23},
]

const userNames = users.map((user)=>{
    return user.firstName;
});

console.log(userNames);