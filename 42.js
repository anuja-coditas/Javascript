// objects inside array 
// very useful in real world applications

const users = [
    {userId: 1,firstName: 'Anuja', gender: 'female'},
    {userId: 2,firstName: 'Shalini', gender: 'female'},
    {userId: 3,firstName: 'Shivam ', gender: 'male'},
]
for(let user of users){
    console.log(user.firstName);
}