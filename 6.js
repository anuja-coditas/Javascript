//string indexing

let firstName = "Anuja";

//  A  n  u  j  a
//  0  1  2  3  4

console.log(firstName[2]);

// length of the string
//firstName.length
console.log(firstName.length);


// last index : length -1
console.log(firstName[firstName.length-2]);



