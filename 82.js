// const userMethods = {
//     about : function(){
//     return `${this.firstName} is ${this.age} years old.`;
//     },
//     is18 : function(){
//         return this.age >= 18;
//     },
//     sing: function(){
//         return 'hooraahhh ';
//     }
// }
function createUser(firstName, lastName, email, age, address){
    const user = Object.create(createUser.prototype);// {}
    user.firstName = firstName;
    user.lastName = lastName;
    user.email = email;
    user.age = age;
    user.address = address;
    return user;
}
createUser.prototype.about = function(){
    return `${this.firstName} is ${this.age} years old.`;
};
createUser.prototype.is18 = function (){
    return this.age >= 18; 
}
createUser.prototype.sing = function (){
    return "yayy hoorraahh ";
}


const user1 = createUser('anuja', 'sharma', 'anuja@gmail.com', 20, "Pune");
const user2 = createUser('shalini', 'sharma', 'anuja@gmail.com', 24, "Jsr");
const user3 = createUser('purvi', 'sharma', 'anuja@gmail.com', 17, "Ranchi");
console.log(user3);
console.log(user3.is18());