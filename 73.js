function about(hobby, favMusician){
    console.log(this.firstName, this.age, hobby, favMusician);
}
const user1 = {
    firstName : "Anuja",
    age: 22,   
}
const user2 = {
    firstName : "Shalini",
    age: 24,
    
}

// apply
about.apply(user1, ["singing", "dandelions"]);
const func = about.bind(user2, "guitar", "bach");
func();