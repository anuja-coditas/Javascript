// object destructuring
const band = {
    bandName: "one direction ",
    famousSong: "Night Changes",
    year: 2000,
    anotherFamousSong: "Perfect",
  };
  
  let { bandName, famousSong, ...restProps } = band;
  console.log(bandName);
  console.log(restProps);