// nested destructuring 
const users = [
    {userId: 1,firstName: 'Anuja', gender: 'female'},
    {userId: 2,firstName: 'Shalini', gender: 'female'},
    {userId: 3,firstName: 'Shivam', gender: 'male'},
]

const [{firstName: user1firstName, userId}, {gender: user3gender}] = users;
console.log(user1firstName);
console.log(userId);
console.log(user3gender);