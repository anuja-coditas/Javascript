// iterables \
// string , array are iterable 

const firstName = "Anuja";
for(let char of firstName){
console.log(char);
}

const items = ['item1', 'item2', 'item3'];
for(let item of items){
console.log(item);
 }

// array like object 

// example :- string 

//firstName = "Anuja";
//console.log(firstName.length);
//console.log(firstName[2]);