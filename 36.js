// objects reference type  
// arrays are good but not sufficient 
// for real world data 
// objects store key value pairs 
// objects don't have index

// how to create objects 

//person = {name:"Anuja",age:22};
const person = {
    name: "Anuja",
    age: 22,
    hobbies: ["Singing", "Travelling", "Baking"]
}
console.log(person);

// how to access data from objects 
console.log(person["name"]);
console.log(person["age"]);
console.log(person.hobbies);

// how to add key value pair to objects
person["person"] = "female";
console.log(person);